package notifier

import (
	"context"
	"github.com/rabbitmq/amqp091-go"
	"go.uber.org/zap"
)

// TODO: add telegram bot api!

func RabbitMQ(ctx context.Context, logger *zap.Logger) error {
	done := make(chan struct{})

	conn, err := amqp091.Dial("amqp://guest:guest@rabbit1:5672/")
	if err != nil {
		logger.Error("Failed to connect to RabbitMQ")
	}

	ch, err := conn.Channel()
	if err != nil {
		logger.Error("Failed to open a channel")
	}

	q, err := ch.QueueDeclare(
		"Crypto_upd",
		false,
		false,
		false,
		false,
		nil)
	if err != nil {
		logger.Error("Failed to declare a queue")
	}
	msgs, err := ch.Consume(
		q.Name,
		"",
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		logger.Error("Failed to register a consumer")
	}

	go func() {
		for d := range msgs {
			logger.Info("Received a message: %s", zap.String(string(d.Body), ""))
		}
	}()
	logger.Info(" [*] Waiting for messages. To exit press CTRL+C")

	select {
	case <-ctx.Done():

	case <-done:

	}

	return nil
}
