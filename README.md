# Go rpc

This is a simple example of how to use rpc in go.
```shell
docker-compose up
```

Links to other services:
```
https://gitlab.com/egory4eff-x/grpc/grpc-user
https://gitlab.com/egory4eff-x/grpc/grpc-crypto
https://gitlab.com/egory4eff-x/grpc/grpc-auth
```

