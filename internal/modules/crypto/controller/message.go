package controller

import "gitlab.com/egory4eff-x/grpc-gateway/internal/models"

type CryptoResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"error_code,omitempty"`
	Data      Data `json:"data"`
}

type Data struct {
	Message string        `json:"message,omitempty"`
	Crypto  []models.Coin `json:"crypto,omitempty"`
}
