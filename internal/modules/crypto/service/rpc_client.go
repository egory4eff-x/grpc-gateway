package service

import (
	"context"
	"gitlab.com/egory4eff-x/grpc-gateway/internal/models"
	"log"
	"net/rpc"
)

type CryptoRPCClient struct {
	client *rpc.Client
}

func NewCryptoRPCClient(client *rpc.Client) *CryptoRPCClient {
	return &CryptoRPCClient{client: client}
}

func (c *CryptoRPCClient) Cost(ctx context.Context) CostOut {
	var out CostOut
	err := c.client.Call("CryptoServiceRPC.Cost", CostOut{}, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (c *CryptoRPCClient) Update(ctx context.Context, coins models.Coins) {
	err := c.client.Call("CryptoServiceRPC.Update", struct{}{}, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func (c *CryptoRPCClient) History(ctx context.Context) HistoryOut {
	var out HistoryOut
	err := c.client.Call("CryptoServiceRPC.History", CostOut{}, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (c *CryptoRPCClient) Max(ctx context.Context) MaxOut {
	var out MaxOut
	err := c.client.Call("CryptoServiceRPC.Max", CostOut{}, &out)
	if err != nil {
		log.Fatal(err)
	}
	return out
}

func (c *CryptoRPCClient) Min(ctx context.Context) MinOut {
	var result MinOut
	err := c.client.Call("CryptoServiceRPC.Min", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}

func (c *CryptoRPCClient) Avg(ctx context.Context) AvgOut {
	var result AvgOut
	err := c.client.Call("CryptoServiceRPC.Avg", CostOut{}, &result)
	if err != nil {
		log.Fatal(err)
	}
	return result
}
