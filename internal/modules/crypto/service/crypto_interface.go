package service

import (
	"context"
	"gitlab.com/egory4eff-x/grpc-gateway/internal/models"
)

type Crypter interface {
	Update(ctx context.Context, models models.Coins)
	Cost(ctx context.Context) CostOut
	History(ctx context.Context) HistoryOut
	Max(ctx context.Context) MaxOut
	Min(ctx context.Context) MinOut
	Avg(ctx context.Context) AvgOut
}

type CostOut struct {
	Coins     []models.Coin `json:"coins"`
	ErrorCode int           `json:"error_code"`
}

type HistoryOut struct {
	Coins     []models.Coin `json:"coins"`
	ErrorCode int           `json:"error_code"`
}

type MaxOut struct {
	Coins     []models.Coin `json:"coins"`
	ErrorCode int           `json:"error_code"`
}

type MinOut struct {
	Coins     []models.Coin `json:"coins"`
	ErrorCode int           `json:"error_code"`
}

type AvgOut struct {
	Coins     []models.Coin `json:"coins"`
	ErrorCode int           `json:"error_code"`
}
