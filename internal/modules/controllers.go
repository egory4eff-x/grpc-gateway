package modules

import (
	"gitlab.com/egory4eff-x/grpc-gateway/internal/infrastructure/component"
	acontroller "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/auth/controller"
	aservice "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/auth/service"
	ccontroller "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/crypto/controller"
	cservice "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/crypto/service"
	ucontroller "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/user/controller"
	uservice "gitlab.com/egory4eff-x/grpc-gateway/internal/modules/user/service"
)

type Controllers struct {
	Auth   acontroller.Auther
	User   ucontroller.Userer
	Crypto ccontroller.Crypter
}

func NewControllers(crypto *cservice.CryptoRPCClient, auth *aservice.AuthServiceJSONRPC, users *uservice.UserServiceJSONRPC, components *component.Components) *Controllers {
	cryptoController := ccontroller.NewCrypto(crypto, components)
	authController := acontroller.NewAuth(auth, components)
	userController := ucontroller.NewUser(users, components)

	return &Controllers{
		Crypto: cryptoController,
		Auth:   authController,
		User:   userController,
	}
}

func NewGRPCControllers(crypto *cservice.CryptoServiceGRPCClient, auth *aservice.AuthServiceGRPC, users *uservice.UserServiceGRPC, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(auth, components)
	cryptoController := ccontroller.NewCrypto(crypto, components)
	usersController := ucontroller.NewUser(users, components)

	return &Controllers{
		Auth:   authController,
		User:   usersController,
		Crypto: cryptoController,
	}
}
