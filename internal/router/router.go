package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/egory4eff-x/grpc-gateway/internal/infrastructure/component"
	"gitlab.com/egory4eff-x/grpc-gateway/internal/infrastructure/middleware"
	"gitlab.com/egory4eff-x/grpc-gateway/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/auth", func(r chi.Router) {
				authController := controllers.Auth
				r.Post("/register", authController.Register)
				r.Post("/login", authController.Login)
				r.Route("/refresh", func(r chi.Router) {
					r.Use(authCheck.CheckRefresh)
					r.Post("/", authController.Refresh)
				})
				r.Post("/verify", authController.Verify)
			})
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
			r.Route("/crypto", func(r chi.Router) {
				cryptoController := controllers.Crypto
				r.Route("/cost", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", cryptoController.Cost)
				})
				r.Route("/history", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", cryptoController.History)
				})

				r.Route("/max", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", cryptoController.Max)
				})

				r.Route("/min", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", cryptoController.Min)
				})

				r.Route("/avg", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", cryptoController.Avg)
				})
			})
		})
	})

	return r
}
